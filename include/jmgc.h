#pragma once
#include <functional>
#include <set>
#include <stdexcept>
#include <unordered_map>
namespace jm {

class gc {
    std::set<void*> roots;
    std::set<void*> objects;
    std::unordered_map<void*, std::unordered_map<void*, int>> references;
    std::unordered_map<void*, int> totalrefs;
    std::unordered_map<void*, std::function<void()>> finalizers;
    std::unordered_map<void*, int> debugids;
    int nextid;
    void assoc(void*, void*);
    void disassoc(void*, void*);
    void collect(void*);
public:
    template <typename T>
    class ref;
    /*  The gc's constructor does nothing but ensure nextid is 0.
     *  All other members are in valid states as default-constructed. */
    gc();
    /*  The gc's destructor takes all its objects with it.
     *  All gc refs are completely invalidated when the gc is freed.
     *  Attempting to use a ref spawned by a dead gc will likely crash. */
    ~gc();
    /*  The gc cannot be copied or moved because it does not know
     *  how to copy or move its objects. To keep things simple,
     *  only destructors are stored, not copy or move constructors. */
    gc(const gc&) = delete;
    gc(gc&&) = delete;
    gc& operator=(const gc&) = delete;
    gc& operator=(gc&&) = delete;
    /*  mkobj
     *  Allocates and initializes an object of arbitrary type
     *  within gc-owned objects, and returns a pointer to the object.
     *  No official references are held to the object at the time of creation,
     *  so it will be collected, and the returned pointer thus invalidated,
     *  if the gc is run prior to ever calling assoc or promote
     *  on the object. */
    template <typename T>
    T* mkobj(const T&);
    /*  mkref
     *  Does some record-keeping to have the object pointed to by void*
     *  capture a reference to the object pointed to by T*,
     *  and then constructs and returns that reference. */
    template <typename T>
    ref<T> mkref(T*, void*);
    /*  owns
     *  Returns true iff void* points to an object managed
     *  by this gc instance. */
    bool owns(void*) const;
    /*  idof
     *  Returns the id of an owned object, or -1 if not owned. */
    int idof(void*) const;
    /*  promote
     *  Causes a managed object to become a root.
     *  Roots are immune to collection,
     *  as orphan determination is conducted relative to them. */
    void promote(void*);
    /*  demote
     *  Causes a managed object to cease to be a root. */
    void demote(void*);
    /*  run
     *  Collects orphaned objects. */
    void run();
    /*  debuginfo
     *  Prints object trees to stderr. */
    void debuginfo();
};

template <typename T>
class gc::ref {
    friend class ::jm::gc;
private:
    T* obj;
    void* scope;
    gc* manager;
    ref(T* obj, void* scope, gc* manager):
        obj(obj), scope(scope), manager(manager) {}
public:
    /*  A ref should, to the greatest extent possible,
     *  act as null when the ref's scope has already been collected.
     *  In theory, a ref should also act as null
     *  when the ref's obj has already been collected,
     *  but this should not happen in the first place
     *  as long as the ref exists. */
    explicit operator bool() const {
        return obj && scope && manager
            && manager->owns(obj)
            && manager->owns(scope);
    }
    /*  Default construction yields an unusable ref.
     *  Calling member functions on it may very well crash.
     *  This constructor is provided only for use with containers. */
    ref(): obj(nullptr), scope(nullptr), manager(nullptr) {}
    /*  When a ref is freed, it triggers gc record-keeping
     *  to relinquish the capture that spawned it.
     *  If this leaves the referenced object with no remaining references,
     *  it will be collected without having to actually run the gc. */
    ~ref() {
        if (operator bool()) {
            manager->disassoc(obj, scope);
        }
    }
    /*  When a ref is copied, it triggers gc record-keeping
     *  to repeat the capture that spawned it. */
    ref(const ref<T>& other):
        obj(other.obj), scope(other.scope), manager(other.manager) {
        manager->assoc(obj, scope);
    }
    ref<T>& operator=(const ref<T>& other) {
        if (operator bool()) {
            manager->disassoc(obj, scope);
        }
        obj = other.obj;
        scope = other.scope;
        manager = other.manager;
        manager->assoc(obj, scope);
    }
    ref(ref<T>&& other) {
        obj = other.obj;
        scope = other.scope;
        manager = other.manager;
        other.obj = nullptr;
        other.scope = nullptr;
        other.manager = nullptr;
    }
    ref<T>& operator=(ref<T>&& other) {
        if (operator bool()) {
            manager->disassoc(obj, scope);
        }
        obj = other.obj;
        scope = other.scope;
        manager = other.manager;
        other.obj = nullptr;
        other.scope = nullptr;
        other.manager = nullptr;
        return *this;
    }
    /*  Refs are treated as equal iff they refer to the same object.
     *  The scope and manager are not considered for this purpose. */
    bool operator==(const ref<T>& other) {
        return obj == other.obj;
    }
    /*  Pointerlike features. */
    operator T*() {
        if (operator bool()) {
            return obj;
        } else {
            return nullptr;
        }
    }
    operator const T*() const {
        if (operator bool()) {
            return obj;
        } else {
            return nullptr;
        }
    }
    T& operator*() {
        return *(static_cast<T*>(*this));
    }
    const T& operator*() const {
        return *(static_cast<const T*>(*this));
    }
    T* operator->() {
        return *this;
    }
    const T* operator->() const {
        return *this;
    }
    /*  gcid
     *  Fetches the referred object's debug ID from the garbage collector. */
    int gcid() const {
        return manager->idof(obj);
    }
};

template <typename T>
T* gc::mkobj(const T& other) {
    T* t = new T(other);
    objects.insert(t);
    finalizers[t] = [t]() { delete t; };
    debugids[t] = nextid++;
    return t;
}

template <typename T>
gc::ref<T> gc::mkref(T* obj, void* scope) {
    if (!owns(obj) || !owns(scope)) {
        throw std::runtime_error(
            "cannot build a gc::ref involving null or unmanaged pointers"
        );
    } else {
        assoc(obj, scope);
        return ref<T>(obj, scope, this);
    }
}

}
