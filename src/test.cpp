#include <jmgc.h>
#include <functional>
#include <iostream>
#include <map>
#include <sstream>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <unordered_map>
#include <utility>
#include <vector>
using namespace std;
using namespace jm;

/*  This file implements a test shell for jmgc. 
 *  I find it somewhat humorous that the code for the test shell
 *  is longer and more complex than the code for jmgc itself.
 *  Such is, I suppose, the nature of a garbage collector,
 *  as a simple but vital algorithm that requires robust unit tests. */

/*  noisemaker
 *  So named for noisily announcing when its destructor is called
 *  and deliberately having no other useful qualities. */
class noisemaker {
private:
    const gc* manager;
public:
    noisemaker(const gc& manager): manager(&manager) {}
    ~noisemaker() {
        int id = manager->idof(this);
        if (id >= 0) {
            cout << "object " << id << " collected" << endl;
        }
    }
};

/*  tty
 *  Needed for sane output depending on whether input is interactive. */
bool tty() {
    return isatty(fileno(stdin));
}

/*  shell
 *  Description of a vaguely sh-like user interface to the object tree. */
class shell {
private:
    struct {
        gc manager;
        noisemaker* root;
        vector<string> workpath;
        unordered_map<void*, map<string, gc::ref<noisemaker>>> vars;
        /*  print_path
         *  Path components in this shell are separated by spaces. */
        static void print_path(
            const vector<string>& path,
            ostream& o = cout,
            vector<string>::size_type from = 0,
            vector<string>::size_type to = -1
        ) {
            if (to == decltype(to)(-1)) {
                to = path.size();
            }
            switch (to - from) {
                case 0: {
                    o << "[top]";
                } break;
                case 1: {
                    o << path[0];
                } break;
                default: {
                    for (decltype(from) i = from; i < to - 1; ++i) {
                        o << path[i] << " ";
                    }
                    o << path[to - 1];
                } break;
            }
        }
        /*  resolve
         *  Follows a path to reach an object. */
        noisemaker* resolve(
            const vector<string>& path,
            vector<string>::size_type from = 0,
            vector<string>::size_type to = -1
        ) {
            if (to == decltype(to)(-1)) {
                to = path.size();
            }
            // start at root
            noisemaker* nm = root;
            for (decltype(from) i = from; i < to; ++i) {
                const string& var = path[i];
                if (vars.count(nm) && vars[nm].count(var)) {
                    // climb the chain
                    nm = vars[nm][var];
                } else {
                    cerr << "[err] nonexistent path '";
                    print_path(path, cerr, from, to);
                    cerr << "'; no such variable "
                        "'" << var << "'" << endl;
                    return nullptr;
                }
            }
            return nm;
        }
        /*  here
         *  Follows the workpath to reach the work object.
         *  Forces sane program state if resolution fails. */
        noisemaker* here() {
            noisemaker* nm = resolve(workpath);
            if (nm) {
                return nm;
            } else {
                cerr << "[err] workpath does not exist; "
                    "kicking back to root" << endl;
                workpath.clear();
                return root;
            }
        }
        /*  link
         *  Adds a reference to the graph. */
        void link(const string& var, noisemaker* nm) {
            noisemaker* where = here();
            int displaced = -1;
            if (vars.count(where) && vars[where].count(var)) {
                displaced = vars[where][var].gcid();
            }
            // add the reference
            // both in gc bookkeeping and in shell bookkeeping
            vars[where][var] = move(manager.mkref(nm, where));
            cout << "gave object " << manager.idof(where) << " (";
            print_path(workpath);
            cout << ") the variable '" << var << "'"
                " referencing object " << manager.idof(nm);
            if (displaced >= 0) {
                cout << " (existing reference to object " << displaced
                    << " was displaced)";
            }
            cout << endl;
        }
        /*  link
         *  Removes a reference from the graph. */
        void unlink(const string& var) {
            noisemaker* where = here();
            int displaced = -1;
            if (vars.count(where) && vars[where].count(var)) {
                displaced = vars[where][var].gcid();
            }
            // remove the reference from shell bookkeeping --
            // the nature of gc::refs should handle gc bookkeeping for us
            // (of course, testing that assumption
            // is the whole point of the shell)
            vars[where].erase(var);
            if (displaced >= 0) {
                cout << "revoked from object " << manager.idof(where) << " (";
                print_path(workpath);
                cout << ") the variable '" << var << "'"
                    " referencing object " << displaced << endl;
            } else {
                cerr << "[err] no such variable '" << var << "'" << endl;
            }
        }
    } state;
    gc& manager;
    noisemaker*& root;
    vector<string>& workpath;
    unordered_map<void*, map<string, gc::ref<noisemaker>>>& vars;
public:
    static map<
        string,
        function<void(decltype(state)&, const vector<string>&)>
    > cmds;
    static unordered_map<string, string> cmddocs;
    /*  getcmd
     *  Read a line from stdin and split it into words.
     *  Do not accept an empty line unless due to stdin hitting eof. */
    static vector<string> getcmd() {
        for (;;) {
            cout << "$ ";
            stringstream ss;
            string s;
            if (!getline(cin, s)) {
                return {};
            }
            if (!tty()) {
                cout << s << endl;
            }
            ss << s;
            vector<string> cmd;
            while (ss >> s) {
                cmd.push_back(s);
            }
            if (!cmd.empty()) {
                return cmd;
            }
        }
    }
    /*  On construction, a shell points its convenience member references
     *  to the corresponding locations in its own guts,
     *  and then adds a root object. */
    shell():
        manager(state.manager),
        root(state.root),
        workpath(state.workpath),
        vars(state.vars) {
        manager.promote(root = manager.mkobj(noisemaker(manager)));
    }
    /*  iter
     *  Display current path, get input,
     *  and perform command lookup and execution.
     *  Return true iff iter should be called again. */
    bool iter() {
        // show path
        noisemaker* here = state.here();
        state.print_path(workpath);
        cout << " (id#" << manager.idof(here) << ")";
        // get input
        vector<string> cmd = getcmd();
        if (cmd.empty()) {
            // if stdin hits eof, we're done here
            cout << endl;
            return false;
        } else if (cmds.count(cmd[0])) {
            // lookup and execute command
            cmds[cmd[0]](state, cmd);
        } else {
            cerr << "[err] unknown command " << cmd[0] << endl;
        }
        return true;
    }
    /*  help
     *  Display command documentation. */
    static void help(const string& what, ostream& o = cout) {
        if (cmddocs.count(what)) {
            o << cmddocs[what] << endl;
        } else if (cmds.count(what)) {
            cerr << "[err] command " << what
                << " is recognized, but appears to be undocumented;"
                << " sorry, this is probably by mistake" << endl;
        } else {
            cerr << "[err] unknown command " << what << endl;
        }
    }
};

// See contents of shell::cmddocs below
// for user-facing / caller-facing documentation of these commands.
decltype(shell::cmds) shell::cmds = {{
    "help", [](auto& sh, const auto& args) {
        switch (args.size()) {
            case 1: {
                // general help
                cout << "try any of the following:" << endl
                    << "overview" << endl;
                for (const auto& kv: shell::cmds) {
                    cout << "help " << kv.first << endl;
                }
            } break;
            case 2: {
                // do doc lookup
                shell::help(args[1]);
            } break;
            default: {
                // wrong arg num
                shell::help("help", cerr);
            } break;
        }
    }
}, {
    "new", [](auto& sh, const auto& args) {
        switch (args.size()) {
            case 2: {
                // make an object and link it in
                sh.link(
                    args[1],
                    sh.manager.mkobj(noisemaker(sh.manager))
                );
            } break;
            default: {
                // wrong arg num
                shell::help("new", cerr);
            } break;
        }
    }
}, {
    "ls", [](auto& sh, const auto& args) {
        switch (args.size()) {
            case 1: {
                // list references held by work obj
                noisemaker* nm = sh.here();
                const auto& vars = sh.vars[nm];
                if (vars.empty()) {
                    cout << "object " << sh.manager.idof(nm) << " (";
                    sh.print_path(sh.workpath);
                    cout << ") has no variables" << endl;
                } else {
                    cout << "variables of object " << sh.manager.idof(nm)
                        << " (";
                    sh.print_path(sh.workpath);
                    cout << "):" << endl;
                    for (const auto& kv: vars) {
                        cout << "'" << kv.first << "': reference to object "
                            << kv.second.gcid() << endl;
                    }
                }
            } break;
            default: {
                // wrong arg num
                shell::help("ls", cerr);
            } break;
        }
    }
}, {
    "rm", [](auto& sh, const auto& args) {
        switch (args.size()) {
            case 2: {
                // unlink given object reference
                sh.unlink(args[1]);
            } break;
            default: {
                // wrong arg num
                shell::help("rm", cerr);
            } break;
        }
    }
}, {
    "ln", [](auto& sh, const auto& args) {
        switch (args.size()) {
            case 0: case 1: case 2: {
                // wrong arg num
                shell::help("ln", cerr);
            } break;
            default: {
                // link-in object reference from elsewhere
                noisemaker* nm = sh.resolve(args, 2);
                if (nm) {
                    sh.link(args[1], nm);
                }
            } break;
        }
    }
}, {
    "in", [](auto& sh, const auto& args) {
        switch (args.size()) {
            case 2: {
                // change work obj
                if (sh.vars[sh.here()].count(args[1])) {
                    sh.workpath.push_back(args[1]);
                } else {
                    cerr << "[err] no such variable '"
                        << args[1] << "'" << endl;
                }
            } break;
            default: {
                // wrong arg num
                shell::help("in", cerr);
            } break;
        }
    }
}, {
    "up", [](auto& sh, const auto& args) {
        switch (args.size()) {
            case 1: {
                // un-change work obj
                if (sh.workpath.empty()) {
                    cerr << "[err] already at top object" << endl;
                } else {
                    sh.workpath.pop_back();
                }
            } break;
            default: {
                // wrong arg num
                shell::help("up", cerr);
            } break;
        }
    }
}, {
    "dump", [](auto& sh, const auto& args) {
        switch (args.size()) {
            case 1: {
                // dump debug info
                cout << "dumping gc debuginfo to stderr" << endl;
                sh.manager.debuginfo();
            } break;
            default: {
                // wrong arg num
                shell::help("dump", cerr);
            } break;
        }
    }
}, {
    "gc", [](auto& sh, const auto& args) {
        switch (args.size()) {
            case 1: {
                // run gc
                sh.manager.run();
            } break;
            default: {
                // wrong arg num
                shell::help("gc", cerr);
            } break;
        }
    }
}, {
    "overview", [](auto& sh, const auto& args) {
        switch (args.size()) {
            case 1: {
                // provide overview
                cout << "jmgc unit test shell: an overview" << endl
                    <<  "I wrote this shell "
                        "to test my garbage collector implementation. "
                        "I was able to identify "
                        "the structure of an object tree "
                        "as similar to that of a filesystem "
                        "with hard links, "
                        "so I designed the shell accordingly." << endl
                    <<  "For ease of parsing, "
                        "paths are separated by spaces instead of slashes. "
                        "There is no quoting. "
                        "Whether a path is absolute or relative "
                        "depends on the context where it's used, "
                        "instead of on its syntax. "
                        "Relative paths are always only one object long. "
                        "So, for instance, the following command..." << endl
                    <<  "$ ln a b c d e" << endl
                    <<  "... would resolve the absolute path 'b c d e' "
                        "and hard-link the located object, if any, "
                        "to the relative path 'a' in the current object." << endl
                    <<  "Type 'help' for a list of more specific help topics."
                    << endl;
            } break;
            default: {
                // wrong arg num
                shell::help("overview", cerr);
            } break;
        }
    }
}};

decltype(shell::cmddocs) shell::cmddocs = {{
    "help",
    "usage: help COMMAND\n"
    "Prints documentation for COMMAND.\n"
    "example: help ls"
}, {
    "new",
    "usage: new RELATIVEPATH\n"
    "Creates an object and gives the current object a reference to it. "
    "Names the reference with the variable name passed as RELATIVEPATH.\n"
    "example: new my-object"
}, {
    "ls",
    "usage: ls\n"
    "Lists the variables of the current object, "
    "and the references held therein. "
    "Does not accept arguments; can only search the current object.\n"
}, {
    "rm",
    "usage: rm RELATIVEPATH\n"
    "Removes from the current object a reference whose variable name "
    "is RELATIVEPATH.\n"
    "example: rm var-to-get-rid-of"
}, {
    "ln",
    "usage: ln RELATIVEPATH ABSOLUTEPATH [...]\n"
    "ABSOLUTEPATH may be arbitrarily many words long. "
    "Resolves ABSOLUTEPATH and gives the current object a reference "
    "called RELATIVEPATH to the object resulting from the path resolution.\n"
    "example: ln my-link-name thing subthing subsubthing"
}, {
    "in",
    "usage: in RELATIVEPATH\n"
    "Enters the scope of the object referred to by RELATIVEPATH. "
    "Analogous to the cd command in Unix-like systems.\n"
    "example: in subobject"
}, {
    "up",
    "usage: up\n"
    "Exits the scope of an object previously entered with 'in.' "
    "Analogous to cd .. in Unix-like systems."
}, {
    "dump",
    "usage: dump\n"
    "Dumps garbage collector debug information."
}, {
    "gc",
    "usage: gc\n"
    "Runs the garbage collector. "
    "If the garbage collector is working properly, "
    "orphaned object rings should report as collected "
    "and there should be no memory leaks and no use-after-free."
}, {
    "overview",
    "usage: overview\n"
    "Provides an overview of the shell, including some key differences "
    "from a Unix-like shell proper. You should invoke this as...\n"
    "$ overview\n"
    "... instead of typing 'help overview.'"
}};

int main() {
    cout << "jmgc unit test shell" << endl;
    if (tty()) {
        cout << "Type 'overview' for an overview of the shell." << endl;
    }
    shell sh;
    while (sh.iter()) {}
    return 0;
}
