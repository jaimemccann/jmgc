#include <jmgc.h>
#include <functional>
#include <iostream>
#include <set>
#include <stack>
#include <stdexcept>
#include <unordered_map>
#include <unordered_set>
using namespace std;
using namespace jm;

void gc::assoc(void* obj, void* scope) {
    if (!owns(obj) || !owns(scope)) {
        throw logic_error(
            "assoc called on null or unmanaged pointers"
        );
    } else {
        auto& held_by_scope = references[scope];
        {
            // create or increment count of references that scope holds to obj
            bool already = held_by_scope.count(obj);
            int& which = held_by_scope[obj];
            if (already) {
                ++which;
            } else {
                which = 1;
            }
        }
        {
            // create or increment count of total references held to obj
            bool already = totalrefs.count(obj);
            int& which = totalrefs[obj];
            if (already) {
                ++which;
            } else {
                which = 1;
            }
        }
    }
}

void gc::disassoc(void* obj, void* scope) {
    if (!owns(obj) || !owns(scope)) {
        throw logic_error(
            "disassoc called on null or unmanaged pointers"
        );
    } else if (references.count(scope)) {
        auto& held_by_scope = references[scope];
        if (held_by_scope.count(obj)) {
            // decrement count of references that scope holds to obj
            if (--held_by_scope[obj] <= 0) {
                held_by_scope.erase(obj);
            }
            // decrement count of total references held to obj --
            // if no refs remain, obj can be collected immediately
            // without a gc run
            if (--totalrefs[obj] <= 0) {
                collect(obj);
            }
        } else {
            throw logic_error(
                "disassoc called for unheld reference"
            );
        }
    } else {
        throw logic_error(
            "disassoc called for unheld reference"
        );
    }
}

void gc::collect(void* obj) {
    if (!owns(obj)) {
        throw logic_error(
            "collect called on null or unmanaged pointer"
        );
    } else {
        // delete object and erase it from all records
        // (assume no reachable references remain without checking here,
        // as the only time collect() should ever be called
        // is when either an object's total reference count drops to 0
        // or the object has already been identified by a gc run
        // as part of a ring of orphans)
        finalizers[obj]();
        objects.erase(obj);
        references.erase(obj);
        totalrefs.erase(obj);
        finalizers.erase(obj);
        debugids.erase(obj);
    }
}

gc::gc(): nextid(0) {}

gc::~gc() {
    // delete all managed objects
    for (const auto& kv: finalizers) {
        kv.second();
    }
}

bool gc::owns(void* obj) const {
    return obj && objects.count(obj);
}

int gc::idof(void* obj) const {
    if (owns(obj)) {
        return debugids.at(obj);
    } else {
        return -1;
    }
}

void gc::promote(void* obj) {
    if (!owns(obj)) {
        throw logic_error(
            "promote called on null or unmanaged pointer"
        );
    } else {
        roots.insert(obj);
    }
}

void gc::demote(void* obj) {
    if (!owns(obj)) {
        throw logic_error(
            "demote called on null or unmanaged pointer"
        );
    } else {
        roots.erase(obj);
    }
}

void gc::run() {
    set<void*> unvisited = objects;
    // multi-root depth-first traversal to scan object graph
    for (void* root: roots) {
        stack<void*> path;
        path.push(root);
        unvisited.erase(root);
        while (!path.empty()) {
            void* current = path.top();
            if (references.count(current)) {
                for (auto kv: references[current]) {
                    if (unvisited.count(kv.first)) {
                        current = kv.first;
                        unvisited.erase(kv.first);
                        break;
                    }
                }
            }
            if (current == path.top()) {
                path.pop();
            } else {
                path.push(current);
            }
        }
    }
    // delete objects not encountered in traversal
    for (void* obj: unvisited) {
        collect(obj);
    }
}

void gc::debuginfo() {
    unordered_map<void*, unordered_set<void*>> printed;
    set<void*> unvisited = objects;
    // lambda to print any given part of the object tree
    auto printref = [&printed, &unvisited, this](
        int tabs,
        void* obj,
        void* scope
    ) {
        // do not re-print identical graph edges
        if (!printed.count(scope) || !printed[scope].count(obj)) {
            printed[scope].insert(obj);
            // indent
            while (tabs-- > 0) {
                cerr << "\t";
            }
            cerr << "object " << debugids[obj];
            // ok to re-print nodes if arrived at by multiple unique edges
            // (we're tracking references, not just objects,
            // so we care about edges, not just nodes)
            if (!unvisited.count(obj)) {
                cerr << " (already visited)";
            }
            cerr << endl;
        }
    };
    // multi-root depth-first traversal to scan object graph
    for (void* root: roots) {
        cerr << "root object " << debugids[root] << endl;
        stack<void*> path;
        path.push(root);
        unvisited.erase(root);
        while (!path.empty()) {
            void* current = path.top();
            if (references.count(current)) {
                for (auto kv: references[current]) {
                    // print the edge even if the node is visited
                    // (if the edge itself isn't yet visited,
                    // it's still important)
                    printref(path.size(), kv.first, current);
                    if (unvisited.count(kv.first)) {
                        current = kv.first;
                        unvisited.erase(kv.first);
                        break;
                    }
                }
            }
            if (current == path.top()) {
                path.pop();
            } else {
                path.push(current);
            }
        }
        cerr << endl;
    }
    // enumerate, but do not delete, objects not encountered in traversal
    for (void* obj: unvisited) {
        cerr << "orphan object " << debugids[obj] << endl;
    }
}
